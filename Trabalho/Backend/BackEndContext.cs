﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;



namespace Backend
{
    public class BackEndContext : DbContext
    {
        public DbSet<Models.Customers> Customers { get; set; }
        public DbSet<Models.Invoices> Invoices { get; set; }
        public DbSet<Models.IVA> IVA { get; set; }
        public DbSet<Models.Products> Products { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=DESKTOP-4SMTIAB\SQLEXPRESS;Database=DevOps;User Id=nandes;Password=123");
        }
    }


}
