﻿using Microsoft.AspNetCore.Routing.Constraints;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Backend.Models
{
    public class Products
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public int Quantity { get; set; }
        public bool Status { get; set; }
        public IVA iva { get; set; }

        public double calcularPrecoTotal(int quantity, double price, double iva)
        {
            return Math.Round(price * quantity * (iva + 1), 2);
        }
    }
}
