﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Backend.Models
{
    public class IVA
    {
        [Key]
        public int idIVA { get; set; }
        public double taxaIVA { get; set; }
        public int estado { get; set; }
        public DateTime dataAtivo { get; set; }
        public DateTime dataInativo { get; set; }



    }
}
