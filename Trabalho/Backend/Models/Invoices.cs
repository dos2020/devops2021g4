﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Backend.Models
{
    public class Invoices
    {
        [Key]

        public int Id { get; set; }
        public int IdCustomer { get; set; }
        public string Code { get; set; } //nome/código/número da fatura tipo FT3000A
        public DateTime InvoiceDate { get; set; } //data na fatura da jeito
        public string Description { get; set; }
        public double Value { get; set; }
        public bool Status { get; set; }
        public double precoTotalIVA { get; set; }

        [ForeignKey("IdCustomer")]
        public Customers customer { get; set; }

        public ICollection<Products> listaProducts { get; set; }

        public double calcularTotalFatura(Invoices inv)
        {

            ICollection<Products> prods = inv.listaProducts;
            double preco = 0.0;
            for (int i = 0; i < prods.Count; i++)
            {
                preco += (prods.ElementAt(i).Quantity * prods.ElementAt(i).Price * (prods.ElementAt(i).iva.taxaIVA + 1));

            }
            return preco;
        }
    }
}
