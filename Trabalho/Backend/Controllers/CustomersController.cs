﻿using System;
using System.Collections.Generic;
using System.Linq;
using Backend.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Backend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CustomersController : ControllerBase
    {

        private BackEndContext _context = new BackEndContext();

        private readonly ILogger<CustomersController> _logger;

        public CustomersController(ILogger<CustomersController> logger)
        {
            _logger = logger;

        }
        [HttpGet]
        public IEnumerable<Customers> Get()
        {
            var customer = _context.Customers.ToList();

            return customer;
        }

        [HttpGet("{id}")]
        public ActionResult<Customers> Get(int id)
        {
            var customer = _context.Customers.FirstOrDefault( (p) => p.Id == id);

            if (customer == null)
            {
                return NotFound();
            }

            return Ok(customer);
        }

        [HttpPost]
        public ActionResult Create(Customers customer)
        {
            try
            {
                customer.Status = true;
                customer.RegisterDate = DateTime.UtcNow;
                _context.Customers.Add(customer);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                BadRequest(ex.Message);
            }

            return Ok(customer);

        }

        [HttpPut("{id}")]
        public ActionResult Update(int id, Customers customer)
        {
            if (id != customer.Id)
            {
                return BadRequest();
            }

            Customers todoCustomer = _context.Customers.FirstOrDefault(x=> x.Id == id);

            if (todoCustomer == null)
            {
                return NotFound();
            }

            todoCustomer.Name = customer.Name;
            todoCustomer.Status = customer.Status;
            _context.Customers.Update(todoCustomer);
            _context.SaveChanges();


            return Ok(todoCustomer);
        }

    }
}
