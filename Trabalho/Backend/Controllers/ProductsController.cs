﻿using System;
using System.Collections.Generic;
using System.Linq;
using Backend.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Backend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductsController : ControllerBase
    {

        private BackEndContext _context = new BackEndContext();


        private readonly ILogger<ProductsController> _logger;

        public ProductsController(ILogger<ProductsController> logger) 
        {
            _logger = logger; 
        }


        // GET: api/<ProductsController>
        //isto é função para ir buscar os produtos
        [HttpGet]
        public IEnumerable<Products> Get()
        {
            var product = _context.Products.ToList();

            return product;
        }

        // GET api/<ProductsController>/5
        [HttpGet("{id}")]
        public ActionResult<Products> Get(int id)
        {
            var product = _context.Products.FirstOrDefault( (p) => p.Id == id  );

            if (product == null) {
                return NotFound();
            }

            return Ok(product); 
        }

        // POST api/<ProductsController>
        [HttpPost]
         public ActionResult Create(Products product)
         {
            try
            {
                product.Status = true;
                product.Quantity += 1; // ao adicionar um produto, incrementar a quantidade do mesmo
                _context.Products.Add(product);
                _context.SaveChanges();

            }
            catch (Exception ex)
            {
            BadRequest(ex.Message);
            }
            return Ok(product);
         }

        // PUT api/<ProductsController>/5
        [HttpPut("{id}")]
        public ActionResult Update (int id, Products product)
        {
            if (id != product.Id) 
            {
                return BadRequest();
            }

            Products todoProduct = _context.Products.FirstOrDefault(x => x.Id == id);

            if (todoProduct == null)
            {
                return NotFound();
            }

            todoProduct.Name = product.Name;
            todoProduct.Status = product.Status;

            _context.Products.Update(todoProduct);
            _context.SaveChanges();

            return Ok(todoProduct);
        }
    }
}
