﻿using System;
using System.Collections.Generic;
using System.Linq;
using Backend.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Backend.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class InvoicesController : ControllerBase
    {

        private BackEndContext _context = new BackEndContext();

        private readonly ILogger<InvoicesController> _logger;

        public InvoicesController(ILogger<InvoicesController> logger)
        {
            _logger = logger;
        }


        // GET: api/<ProductsController>
        [HttpGet]
        public IEnumerable<Invoices> Get()
        {
            var invoice = _context.Invoices.ToList();

            return invoice;
        }

        // GET api/<ProductsController>/5
        [HttpGet("{id}")]
        public ActionResult<Invoices> Get(int id)
        {
            var invoice = _context.Invoices.FirstOrDefault((p) => p.Id == id);

            if (invoice == null)
            {
                return NotFound();
            }

            return Ok(invoice);
        }

        // POST api/<ProductsController>
        [HttpPost]
        public ActionResult Create(Invoices invoice)
        {
            try
            {
                invoice.Status = true;
                invoice.InvoiceDate = DateTime.UtcNow;
                _context.Invoices.Add(invoice);
                _context.SaveChanges();

            }
            catch (Exception ex)
            {
                BadRequest(ex.Message);
            }

            return Ok(invoice);
        }

        // PUT api/<ProductsController>/5
        [HttpPut("{id}")]
        public ActionResult Update(int id, Invoices invoice)
        {
            if (id != invoice.Id)
            {
                return BadRequest();
            }

            Invoices todoInvoice = _context.Invoices.FirstOrDefault(x => x.Id == id);

            if (todoInvoice == null)
            {
                return NotFound();
            }

            todoInvoice.Code = invoice.Code;
            todoInvoice.Status = invoice.Status;
            todoInvoice.Description = invoice.Description;

            _context.Invoices.Update(todoInvoice);
            _context.SaveChanges();

            return Ok(todoInvoice);
        }
    }
}
