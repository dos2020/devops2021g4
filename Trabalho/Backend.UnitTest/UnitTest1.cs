using Backend.Models;
using System;
using System.Collections.Generic;
using System.Reflection.Metadata;
using Xunit;

namespace Backend.UnitTest
{
public class UnitTest1
{
[Fact]
public void testeCalcularLinha()
{
    Products prd = new Products();

    double result = prd.calcularPrecoTotal(1, 2, 0.23);
    Assert.Equal(2.46, result);

}
[Fact]
public void testeVerifLinhaPrecoTotal()
{
    Invoices teste = new Invoices
    {
        Id = 1,
        Code = "11111",
        InvoiceDate = Convert.ToDateTime("2020-03-03"),
        Description = "teste",
        Value = 1,
        Status = true,
        precoTotalIVA = 1.23,
        listaProducts = new List<Products> {
            new Products {
                Id = 1,
                Name = "Asus ROG Strix GeForce RTX 3080 10GB GDDR6X OC Edition",
                Description = "Nvidia RTX 3080 Ampere Graphics Card 10GB VRAM with factory overclock version",
                Price = 920.90 ,
                Quantity = 3 ,
                Status = true,
                iva = new IVA { 
                    taxaIVA =  0.23, 
                    estado = 1, 
                    dataAtivo = Convert.ToDateTime("2020-03-03"), 
                    dataInativo = Convert.ToDateTime("2020-03-03") 
                }
            },
            new Products {
                Id = 2,
                Name = "AMD Ryzen 5800x",
                Description = "AMD Ryzen 7 5800X 8-Core 3.8GHz c/ Turbo 4.7GHz 36MB SktAM4",
                Price = 469.90,
                Quantity = 10,
                Status = true,
                iva = new IVA {
                    taxaIVA =  0.23,
                    estado = 1,
                    dataAtivo = Convert.ToDateTime("2020-03-03"),
                    dataInativo = Convert.ToDateTime("2020-03-03")
                }
            }
        }
    };


    double result = teste.calcularTotalFatura(teste);
    Assert.Equal(9177.891, result);

}
}
}
